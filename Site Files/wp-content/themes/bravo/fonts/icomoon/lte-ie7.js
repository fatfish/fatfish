/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
			'ss-home' : '&#xe000;',
			'ss-home-2' : '&#xe001;',
			'ss-home-3' : '&#xe002;',
			'ss-office' : '&#xe003;',
			'ss-newspaper' : '&#xe004;',
			'ss-pencil' : '&#xe005;',
			'ss-pencil-2' : '&#xe006;',
			'ss-quill' : '&#xe007;',
			'ss-pen' : '&#xe008;',
			'ss-blog' : '&#xe009;',
			'ss-droplet' : '&#xe00a;',
			'ss-paint-format' : '&#xe00b;',
			'ss-image' : '&#xe00c;',
			'ss-image-2' : '&#xe00d;',
			'ss-images' : '&#xe00e;',
			'ss-camera' : '&#xe00f;',
			'ss-music' : '&#xe010;',
			'ss-headphones' : '&#xe011;',
			'ss-play' : '&#xe012;',
			'ss-film' : '&#xe013;',
			'ss-camera-2' : '&#xe014;',
			'ss-dice' : '&#xe015;',
			'ss-pacman' : '&#xe016;',
			'ss-spades' : '&#xe017;',
			'ss-clubs' : '&#xe018;',
			'ss-diamonds' : '&#xe019;',
			'ss-pawn' : '&#xe01a;',
			'ss-bullhorn' : '&#xe01b;',
			'ss-connection' : '&#xe01c;',
			'ss-podcast' : '&#xe01d;',
			'ss-feed' : '&#xe01e;',
			'ss-book' : '&#xe01f;',
			'ss-books' : '&#xe020;',
			'ss-library' : '&#xe021;',
			'ss-file' : '&#xe022;',
			'ss-profile' : '&#xe023;',
			'ss-file-2' : '&#xe024;',
			'ss-file-3' : '&#xe025;',
			'ss-file-4' : '&#xe026;',
			'ss-copy' : '&#xe027;',
			'ss-copy-2' : '&#xe028;',
			'ss-copy-3' : '&#xe029;',
			'ss-paste' : '&#xe02a;',
			'ss-paste-2' : '&#xe02b;',
			'ss-paste-3' : '&#xe02c;',
			'ss-stack' : '&#xe02d;',
			'ss-folder' : '&#xe02e;',
			'ss-folder-open' : '&#xe02f;',
			'ss-tag' : '&#xe030;',
			'ss-tags' : '&#xe031;',
			'ss-barcode' : '&#xe032;',
			'ss-qrcode' : '&#xe033;',
			'ss-ticket' : '&#xe034;',
			'ss-cart' : '&#xe035;',
			'ss-cart-2' : '&#xe036;',
			'ss-cart-3' : '&#xe037;',
			'ss-coin' : '&#xe038;',
			'ss-credit' : '&#xe039;',
			'ss-calculate' : '&#xe03a;',
			'ss-support' : '&#xe03b;',
			'ss-phone' : '&#xe03c;',
			'ss-phone-hang-up' : '&#xe03d;',
			'ss-address-book' : '&#xe03e;',
			'ss-notebook' : '&#xe03f;',
			'ss-envelop' : '&#xe040;',
			'ss-pushpin' : '&#xe041;',
			'ss-location' : '&#xe042;',
			'ss-location-2' : '&#xe043;',
			'ss-compass' : '&#xe044;',
			'ss-map' : '&#xe045;',
			'ss-map-2' : '&#xe046;',
			'ss-history' : '&#xe047;',
			'ss-clock' : '&#xe048;',
			'ss-clock-2' : '&#xe049;',
			'ss-alarm' : '&#xe04a;',
			'ss-alarm-2' : '&#xe04b;',
			'ss-bell' : '&#xe04c;',
			'ss-stopwatch' : '&#xe04d;',
			'ss-calendar' : '&#xe04e;',
			'ss-calendar-2' : '&#xe04f;',
			'ss-print' : '&#xe050;',
			'ss-keyboard' : '&#xe051;',
			'ss-screen' : '&#xe052;',
			'ss-laptop' : '&#xe053;',
			'ss-mobile' : '&#xe054;',
			'ss-mobile-2' : '&#xe055;',
			'ss-tablet' : '&#xe056;',
			'ss-tv' : '&#xe057;',
			'ss-cabinet' : '&#xe058;',
			'ss-drawer' : '&#xe059;',
			'ss-drawer-2' : '&#xe05a;',
			'ss-drawer-3' : '&#xe05b;',
			'ss-box-add' : '&#xe05c;',
			'ss-box-remove' : '&#xe05d;',
			'ss-download' : '&#xe05e;',
			'ss-upload' : '&#xe05f;',
			'ss-disk' : '&#xe060;',
			'ss-storage' : '&#xe061;',
			'ss-undo' : '&#xe062;',
			'ss-redo' : '&#xe063;',
			'ss-flip' : '&#xe064;',
			'ss-flip-2' : '&#xe065;',
			'ss-undo-2' : '&#xe066;',
			'ss-redo-2' : '&#xe067;',
			'ss-forward' : '&#xe068;',
			'ss-reply' : '&#xe069;',
			'ss-bubble' : '&#xe06a;',
			'ss-bubbles' : '&#xe06b;',
			'ss-bubbles-2' : '&#xe06c;',
			'ss-bubble-2' : '&#xe06d;',
			'ss-bubbles-3' : '&#xe06e;',
			'ss-bubbles-4' : '&#xe06f;',
			'ss-user' : '&#xe070;',
			'ss-users' : '&#xe071;',
			'ss-user-2' : '&#xe072;',
			'ss-users-2' : '&#xe073;',
			'ss-user-3' : '&#xe074;',
			'ss-user-4' : '&#xe075;',
			'ss-quotes-left' : '&#xe076;',
			'ss-busy' : '&#xe077;',
			'ss-spinner' : '&#xe078;',
			'ss-spinner-2' : '&#xe079;',
			'ss-spinner-3' : '&#xe07a;',
			'ss-spinner-4' : '&#xe07b;',
			'ss-spinner-5' : '&#xe07c;',
			'ss-spinner-6' : '&#xe07d;',
			'ss-binoculars' : '&#xe07e;',
			'ss-search' : '&#xe07f;',
			'ss-zoom-in' : '&#xe080;',
			'ss-zoom-out' : '&#xe081;',
			'ss-expand' : '&#xe082;',
			'ss-contract' : '&#xe083;',
			'ss-expand-2' : '&#xe084;',
			'ss-contract-2' : '&#xe085;',
			'ss-key' : '&#xe086;',
			'ss-key-2' : '&#xe087;',
			'ss-lock' : '&#xe088;',
			'ss-lock-2' : '&#xe089;',
			'ss-unlocked' : '&#xe08a;',
			'ss-wrench' : '&#xe08b;',
			'ss-settings' : '&#xe08c;',
			'ss-equalizer' : '&#xe08d;',
			'ss-cog' : '&#xe08e;',
			'ss-cogs' : '&#xe08f;',
			'ss-cog-2' : '&#xe090;',
			'ss-hammer' : '&#xe091;',
			'ss-wand' : '&#xe092;',
			'ss-aid' : '&#xe093;',
			'ss-bug' : '&#xe094;',
			'ss-pie' : '&#xe095;',
			'ss-stats' : '&#xe096;',
			'ss-bars' : '&#xe097;',
			'ss-bars-2' : '&#xe098;',
			'ss-gift' : '&#xe099;',
			'ss-trophy' : '&#xe09a;',
			'ss-glass' : '&#xe09b;',
			'ss-mug' : '&#xe09c;',
			'ss-food' : '&#xe09d;',
			'ss-leaf' : '&#xe09e;',
			'ss-rocket' : '&#xe09f;',
			'ss-meter' : '&#xe0a0;',
			'ss-meter2' : '&#xe0a1;',
			'ss-dashboard' : '&#xe0a2;',
			'ss-hammer-2' : '&#xe0a3;',
			'ss-fire' : '&#xe0a4;',
			'ss-lab' : '&#xe0a5;',
			'ss-magnet' : '&#xe0a6;',
			'ss-remove' : '&#xe0a7;',
			'ss-remove-2' : '&#xe0a8;',
			'ss-briefcase' : '&#xe0a9;',
			'ss-airplane' : '&#xe0aa;',
			'ss-truck' : '&#xe0ab;',
			'ss-road' : '&#xe0ac;',
			'ss-accessibility' : '&#xe0ad;',
			'ss-target' : '&#xe0ae;',
			'ss-shield' : '&#xe0af;',
			'ss-lightning' : '&#xe0b0;',
			'ss-switch' : '&#xe0b1;',
			'ss-power-cord' : '&#xe0b2;',
			'ss-signup' : '&#xe0b3;',
			'ss-list' : '&#xe0b4;',
			'ss-list-2' : '&#xe0b5;',
			'ss-numbered-list' : '&#xe0b6;',
			'ss-menu' : '&#xe0b7;',
			'ss-menu-2' : '&#xe0b8;',
			'ss-tree' : '&#xe0b9;',
			'ss-cloud' : '&#xe0ba;',
			'ss-cloud-download' : '&#xe0bb;',
			'ss-cloud-upload' : '&#xe0bc;',
			'ss-download-2' : '&#xe0bd;',
			'ss-upload-2' : '&#xe0be;',
			'ss-download-3' : '&#xe0bf;',
			'ss-upload-3' : '&#xe0c0;',
			'ss-globe' : '&#xe0c1;',
			'ss-earth' : '&#xe0c2;',
			'ss-link' : '&#xe0c3;',
			'ss-flag' : '&#xe0c4;',
			'ss-attachment' : '&#xe0c5;',
			'ss-eye' : '&#xe0c6;',
			'ss-eye-blocked' : '&#xe0c7;',
			'ss-eye-2' : '&#xe0c8;',
			'ss-bookmark' : '&#xe0c9;',
			'ss-bookmarks' : '&#xe0ca;',
			'ss-brightness-medium' : '&#xe0cb;',
			'ss-brightness-contrast' : '&#xe0cc;',
			'ss-contrast' : '&#xe0cd;',
			'ss-star' : '&#xe0ce;',
			'ss-star-2' : '&#xe0cf;',
			'ss-star-3' : '&#xe0d0;',
			'ss-heart' : '&#xe0d1;',
			'ss-heart-2' : '&#xe0d2;',
			'ss-heart-broken' : '&#xe0d3;',
			'ss-thumbs-up' : '&#xe0d4;',
			'ss-thumbs-up-2' : '&#xe0d5;',
			'ss-happy' : '&#xe0d6;',
			'ss-happy-2' : '&#xe0d7;',
			'ss-smiley' : '&#xe0d8;',
			'ss-smiley-2' : '&#xe0d9;',
			'ss-tongue' : '&#xe0da;',
			'ss-tongue-2' : '&#xe0db;',
			'ss-sad' : '&#xe0dc;',
			'ss-sad-2' : '&#xe0dd;',
			'ss-wink' : '&#xe0de;',
			'ss-wink-2' : '&#xe0df;',
			'ss-grin' : '&#xe0e0;',
			'ss-grin-2' : '&#xe0e1;',
			'ss-cool' : '&#xe0e2;',
			'ss-cool-2' : '&#xe0e3;',
			'ss-angry' : '&#xe0e4;',
			'ss-angry-2' : '&#xe0e5;',
			'ss-evil' : '&#xe0e6;',
			'ss-evil-2' : '&#xe0e7;',
			'ss-shocked' : '&#xe0e8;',
			'ss-shocked-2' : '&#xe0e9;',
			'ss-confused' : '&#xe0ea;',
			'ss-confused-2' : '&#xe0eb;',
			'ss-neutral' : '&#xe0ec;',
			'ss-neutral-2' : '&#xe0ed;',
			'ss-wondering' : '&#xe0ee;',
			'ss-wondering-2' : '&#xe0ef;',
			'ss-point-up' : '&#xe0f0;',
			'ss-point-right' : '&#xe0f1;',
			'ss-point-down' : '&#xe0f2;',
			'ss-point-left' : '&#xe0f3;',
			'ss-warning' : '&#xe0f4;',
			'ss-notification' : '&#xe0f5;',
			'ss-question' : '&#xe0f6;',
			'ss-info' : '&#xe0f7;',
			'ss-info-2' : '&#xe0f8;',
			'ss-blocked' : '&#xe0f9;',
			'ss-cancel-circle' : '&#xe0fa;',
			'ss-checkmark-circle' : '&#xe0fb;',
			'ss-spam' : '&#xe0fc;',
			'ss-close' : '&#xe0fd;',
			'ss-checkmark' : '&#xe0fe;',
			'ss-checkmark-2' : '&#xe0ff;',
			'ss-spell-check' : '&#xe100;',
			'ss-minus' : '&#xe101;',
			'ss-plus' : '&#xe102;',
			'ss-enter' : '&#xe103;',
			'ss-exit' : '&#xe104;',
			'ss-play-2' : '&#xe105;',
			'ss-pause' : '&#xe106;',
			'ss-stop' : '&#xe107;',
			'ss-backward' : '&#xe108;',
			'ss-forward-2' : '&#xe109;',
			'ss-play-3' : '&#xe10a;',
			'ss-pause-2' : '&#xe10b;',
			'ss-stop-2' : '&#xe10c;',
			'ss-backward-2' : '&#xe10d;',
			'ss-forward-3' : '&#xe10e;',
			'ss-first' : '&#xe10f;',
			'ss-last' : '&#xe110;',
			'ss-previous' : '&#xe111;',
			'ss-next' : '&#xe112;',
			'ss-eject' : '&#xe113;',
			'ss-volume-high' : '&#xe114;',
			'ss-volume-medium' : '&#xe115;',
			'ss-volume-low' : '&#xe116;',
			'ss-volume-mute' : '&#xe117;',
			'ss-volume-mute-2' : '&#xe118;',
			'ss-volume-increase' : '&#xe119;',
			'ss-volume-decrease' : '&#xe11a;',
			'ss-loop' : '&#xe11b;',
			'ss-loop-2' : '&#xe11c;',
			'ss-loop-3' : '&#xe11d;',
			'ss-shuffle' : '&#xe11e;',
			'ss-arrow-up-left' : '&#xe11f;',
			'ss-arrow-up' : '&#xe120;',
			'ss-arrow-up-right' : '&#xe121;',
			'ss-arrow-right' : '&#xe122;',
			'ss-arrow-down-right' : '&#xe123;',
			'ss-arrow-down' : '&#xe124;',
			'ss-arrow-down-left' : '&#xe125;',
			'ss-arrow-left' : '&#xe126;',
			'ss-arrow-up-left-2' : '&#xe127;',
			'ss-arrow-up-2' : '&#xe128;',
			'ss-arrow-up-right-2' : '&#xe129;',
			'ss-arrow-right-2' : '&#xe12a;',
			'ss-arrow-down-right-2' : '&#xe12b;',
			'ss-arrow-down-2' : '&#xe12c;',
			'ss-arrow-down-left-2' : '&#xe12d;',
			'ss-arrow-left-2' : '&#xe12e;',
			'ss-arrow-up-left-3' : '&#xe12f;',
			'ss-arrow-up-3' : '&#xe130;',
			'ss-arrow-up-right-3' : '&#xe131;',
			'ss-arrow-right-3' : '&#xe132;',
			'ss-arrow-down-right-3' : '&#xe133;',
			'ss-arrow-down-3' : '&#xe134;',
			'ss-arrow-down-left-3' : '&#xe135;',
			'ss-arrow-left-3' : '&#xe136;',
			'ss-tab' : '&#xe137;',
			'ss-checkbox-checked' : '&#xe138;',
			'ss-checkbox-unchecked' : '&#xe139;',
			'ss-checkbox-partial' : '&#xe13a;',
			'ss-radio-checked' : '&#xe13b;',
			'ss-radio-unchecked' : '&#xe13c;',
			'ss-crop' : '&#xe13d;',
			'ss-scissors' : '&#xe13e;',
			'ss-filter' : '&#xe13f;',
			'ss-filter-2' : '&#xe140;',
			'ss-font' : '&#xe141;',
			'ss-text-height' : '&#xe142;',
			'ss-text-width' : '&#xe143;',
			'ss-bold' : '&#xe144;',
			'ss-underline' : '&#xe145;',
			'ss-italic' : '&#xe146;',
			'ss-strikethrough' : '&#xe147;',
			'ss-omega' : '&#xe148;',
			'ss-sigma' : '&#xe149;',
			'ss-table' : '&#xe14a;',
			'ss-table-2' : '&#xe14b;',
			'ss-insert-template' : '&#xe14c;',
			'ss-pilcrow' : '&#xe14d;',
			'ss-left-to-right' : '&#xe14e;',
			'ss-right-to-left' : '&#xe14f;',
			'ss-paragraph-left' : '&#xe150;',
			'ss-paragraph-center' : '&#xe151;',
			'ss-paragraph-right' : '&#xe152;',
			'ss-paragraph-justify' : '&#xe153;',
			'ss-paragraph-left-2' : '&#xe154;',
			'ss-paragraph-center-2' : '&#xe155;',
			'ss-paragraph-right-2' : '&#xe156;',
			'ss-paragraph-justify-2' : '&#xe157;',
			'ss-indent-increase' : '&#xe158;',
			'ss-indent-decrease' : '&#xe159;',
			'ss-new-tab' : '&#xe15a;',
			'ss-embed' : '&#xe15b;',
			'ss-code' : '&#xe15c;',
			'ss-console' : '&#xe15d;',
			'ss-share' : '&#xe15e;',
			'ss-mail' : '&#xe15f;',
			'ss-mail-2' : '&#xe160;',
			'ss-mail-3' : '&#xe161;',
			'ss-mail-4' : '&#xe162;',
			'ss-google' : '&#xe163;',
			'ss-google-plus' : '&#xe164;',
			'ss-google-plus-2' : '&#xe165;',
			'ss-google-plus-3' : '&#xe166;',
			'ss-google-plus-4' : '&#xe167;',
			'ss-google-drive' : '&#xe168;',
			'ss-facebook' : '&#xe169;',
			'ss-facebook-2' : '&#xe16a;',
			'ss-facebook-3' : '&#xe16b;',
			'ss-instagram' : '&#xe16c;',
			'ss-twitter' : '&#xe16d;',
			'ss-twitter-2' : '&#xe16e;',
			'ss-twitter-3' : '&#xe16f;',
			'ss-feed-2' : '&#xe170;',
			'ss-feed-3' : '&#xe171;',
			'ss-feed-4' : '&#xe172;',
			'ss-youtube' : '&#xe173;',
			'ss-youtube-2' : '&#xe174;',
			'ss-vimeo' : '&#xe175;',
			'ss-vimeo2' : '&#xe176;',
			'ss-vimeo-2' : '&#xe177;',
			'ss-lanyrd' : '&#xe178;',
			'ss-flickr' : '&#xe179;',
			'ss-flickr-2' : '&#xe17a;',
			'ss-flickr-3' : '&#xe17b;',
			'ss-flickr-4' : '&#xe17c;',
			'ss-picassa' : '&#xe17d;',
			'ss-picassa-2' : '&#xe17e;',
			'ss-dribbble' : '&#xe17f;',
			'ss-dribbble-2' : '&#xe180;',
			'ss-dribbble-3' : '&#xe181;',
			'ss-forrst' : '&#xe182;',
			'ss-forrst-2' : '&#xe183;',
			'ss-deviantart' : '&#xe184;',
			'ss-deviantart-2' : '&#xe185;',
			'ss-steam' : '&#xe186;',
			'ss-steam-2' : '&#xe187;',
			'ss-github' : '&#xe188;',
			'ss-github-2' : '&#xe189;',
			'ss-github-3' : '&#xe18a;',
			'ss-github-4' : '&#xe18b;',
			'ss-github-5' : '&#xe18c;',
			'ss-wordpress' : '&#xe18d;',
			'ss-wordpress-2' : '&#xe18e;',
			'ss-joomla' : '&#xe18f;',
			'ss-blogger' : '&#xe190;',
			'ss-blogger-2' : '&#xe191;',
			'ss-tumblr' : '&#xe192;',
			'ss-tumblr-2' : '&#xe193;',
			'ss-yahoo' : '&#xe194;',
			'ss-tux' : '&#xe195;',
			'ss-apple' : '&#xe196;',
			'ss-finder' : '&#xe197;',
			'ss-android' : '&#xe198;',
			'ss-windows' : '&#xe199;',
			'ss-windows8' : '&#xe19a;',
			'ss-soundcloud' : '&#xe19b;',
			'ss-soundcloud-2' : '&#xe19c;',
			'ss-skype' : '&#xe19d;',
			'ss-reddit' : '&#xe19e;',
			'ss-linkedin' : '&#xe19f;',
			'ss-lastfm' : '&#xe1a0;',
			'ss-lastfm-2' : '&#xe1a1;',
			'ss-delicious' : '&#xe1a2;',
			'ss-stumbleupon' : '&#xe1a3;',
			'ss-stumbleupon-2' : '&#xe1a4;',
			'ss-stackoverflow' : '&#xe1a5;',
			'ss-pinterest' : '&#xe1a6;',
			'ss-pinterest-2' : '&#xe1a7;',
			'ss-xing' : '&#xe1a8;',
			'ss-xing-2' : '&#xe1a9;',
			'ss-flattr' : '&#xe1aa;',
			'ss-foursquare' : '&#xe1ab;',
			'ss-foursquare-2' : '&#xe1ac;',
			'ss-paypal' : '&#xe1ad;',
			'ss-paypal-2' : '&#xe1ae;',
			'ss-paypal-3' : '&#xe1af;',
			'ss-yelp' : '&#xe1b0;',
			'ss-libreoffice' : '&#xe1b1;',
			'ss-file-pdf' : '&#xe1b2;',
			'ss-file-openoffice' : '&#xe1b3;',
			'ss-file-word' : '&#xe1b4;',
			'ss-file-excel' : '&#xe1b5;',
			'ss-file-zip' : '&#xe1b6;',
			'ss-file-powerpoint' : '&#xe1b7;',
			'ss-file-xml' : '&#xe1b8;',
			'ss-file-css' : '&#xe1b9;',
			'ss-html5' : '&#xe1ba;',
			'ss-html5-2' : '&#xe1bb;',
			'ss-css3' : '&#xe1bc;',
			'ss-chrome' : '&#xe1bd;',
			'ss-firefox' : '&#xe1be;',
			'ss-IE' : '&#xe1bf;',
			'ss-opera' : '&#xe1c0;',
			'ss-safari' : '&#xe1c1;',
			'ss-IcoMoon' : '&#xe1c2;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/ss-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};