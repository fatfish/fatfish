= Bravo =

* by the Prothemeus, http://www.prothemeus.com/

== ABOUT Bravo ==

Bravo is a full screen, responsive wordpress theme with an AJAX based portfolio section. It has a very unique design, looks stylish & modern and has powerful features packed into it to help you give your visitors a stunning visual experience. 

WORDPRESS 3.5 COMPATIBLE

FEATURES:    

Filterable Circular Portfolio -  A unique "never seen before" styling, that is stylish yet highly functional. The portfolio is full screen and beautifully adapts to different screen sizes. Looks stunning on a 21inch iMac as well as 4 inch smartphone.  Small & Big - Two circle size options to choose from. 

Filterable Rectangular Portfolio - If you want more room for your thumbnails along with the Title of the portfolio, then choose this variation. Comes in a 2 sizes as well and is functionally the same as the circular variant. 

Portfolio's support INFINITE SCROLL, so that worry about having a large number of portfolio items, Bravo will still be quick for a great user experience. Standard Pagination is supported as well. 

CATEGORIZE your Portfolio items, showcase in individual pages and still filter by tags, or publish all the items in one page and filter by categories / tags. Bravo is functionally flexible as well!

Two Gallery styles - NO boring lightboxes !! Bravo comes with cool AJAX galleries for your portfolio. Supports swipe gestures on touch devices and can have both photos and videos (youtube & vimeo) . Choose between two styles - A centered gallery with a max height & width of the size of the screen and a full screen variant. The galleries have nonintrusive thumbnails too. 

Unique Page styles - A layout for the content pages that gives you a lot of room to present both your content and your stunning background. Oh did I tell you that you can set a unique background for each page ? Iconized widgets that slide out when you need them.

CSS3 animations - for super smooth performance in modern browsers and a jquery fallback for the oldies. 

Two Layout styles - Cornered or Centered - See the Demo

Comprehensive options panel - The right set of options presented with simple controls to get you up and running in a flash

Choose from over 500 google fonts and control the typography of each section . 

Over 20 shortcodes - easy to add them to your page with a Tiny MCE button. No need to have the documentation beside you. 

Bravo is thoroughly documented and comes with great customer support.  We have taken a lot of effort to develop this theme and are hell bent on making it successful. We understand how important it is satisfy you and we are committed to keep the theme updated and offer quality support. Our support is team is fast and actionable. We will get into your site and make hot fixes if required. 

Support via email and the comments section here on themeforest. We can do 5-10 minute customizations for free and we are available to make further customizations at $20 / hr.